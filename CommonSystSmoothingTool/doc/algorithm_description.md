# Description of the different smoothing algorithms

Several algorithms are available for smoothing, each has its advantaged and disadvantages.
Generally speaking, if smoothing is important in your analysis, it is useful to try different smoothing methods and check their impact on the result as there is no "one algorithm to rule them all".

A summary presentation about some of the algorithms can be found [here](https://indico.cern.ch/event/736395/contributions/3040869/attachments/1671620/2684026/SmoothingPruning.pdf).

## Rebinning algorithms

The simplest algorithms rely on rebinning the input distributions of the systematic variation to achieve smooth behaviour.
These are: `smoothRebinParabolic` and `smoothRebinMonotonic`.

The algorithm is as follow:
 1) If the MC statistical uncertianty of the nominal is larger than 5% of the total yield, rebin to 1 fianl bin (no shape)
 2) Merge bins that are most compatible until only two (three) extrema are found for `smoothRebinMonotonic` (`smoothRebinParabolic`) setting.
   * Extrema means a bin that is different than the neighbouging one
   * Most compatible bins are estimated by taking the chi^2 between the average of the neighbouring bins and the considered bin
 3) Then rebin until MC stat. uncertainty is smaller than 5% in all bins

The algorithm is ilustrated in the picture below
![Rebinning algorithm](Rebin.png)

## Kernel algorithms

Kernel smoothing algorithms modify the value of point `x0`, `y(x0)` with
```math
\hat{y}(x_0) = \frac{\sum_{i\in bins}K(x_0,x_i)y(x_i)}{\sum_{i\in bins}K(x_0,x_i)},
```

where `K` is a kernel function

```math
K(x_0,x_i) = f\left(\frac{x_0 - x_i}{h}\right),
```

with a radius parameter `h`.
Two supported kernel functions are provided, the uniform (`*UniformKernel`) and gaussian (`*GaussKernel`) kernels.
There are two smoothing versions provided, one that smooths the difference between the systematic variation and the nominal (`smoothDelta*`) and one that smooths the ratio of the systematic variation and the nominal (`smoothRatio*`).
The radius parameter is a free parameter of the kernel, however, a dedicated technique is used to find its optimal value.
The algorithm is called "leave-one-out-cross-validation" and it is as follows.
1) Loop over all bins (loop over `i`)
2) Loop over all predefined values of the radius parameter `h`
3) Smooth histograms (using the kerner formula) _excluding_ the point i
4) Calculate the residual `d` of all points wrt the smoothed points
```math
d = \sum (s_j - n_j)^2,
```
where `s_j` is the value of the smoothed bin and `n_j` is the original bin `j`.

5) Find value of `h_i` that has a minimal residual for a given bin `i`
6) Use `h` as
```math
h = \sum h_i / n,
```

where `n` is the number of points (bins).

The algorithm is ilustrated in the picture below
![Kernel algorithm](Kernel.png)

For more details, check [these](https://indico.cern.ch/event/609432/contributions/2458464/attachments/1403868/2144107/KernelSmoothing.pdf) and [these](https://indico.cern.ch/event/616857/contributions/2490217/attachments/1419051/2173790/UniformKernelSmoothing.pdf) slides.

## Rebinning + TH1::Smooth (TRExFitter)

### TRExFitter Default (MaxVariation)

The algorithm limits the number of variations in the ratio (syst/nominal) histogram, by using the following algorithm:
1) Rebin the distribution until the relative MC stat uncertainty is below a predefined tolerance
2) If number of variations (slopes) in the ratio plot is small than a user defined maximum N, halve the tolerance and repeat step 1)
3) When the algorithm finishes, run the TH1::Smooth smoothing that uses the "353QH twice" algorithm (Friedman in Proc.of the 1974 CERN School of Computing, Norway, 11-24 August, 1974.) to avoid artifically flat uncertainties due to rebinning

### TTbarResonance smoothing

This algorithm has two variations, for correlated (e.g. scale variations) and uncorrelated (e.g. different generators) sources of the events.
Firstly, a threshold is defined for the correlated case

```math
\delta M = max(\delta S, \delta N),
```

and for the uncorrelated case

```math
\sqrt{\delta S^2 + \delta N^2},
```

where `\delta S` represents the MC stat uncertainty of the systematic variation and `\delta N` denotes the MC stat uncertainty for the nominal histogram.

Then, for each bin, `i`, and its neighbour, calculate

```math
x_{i-1,i} = \left|\frac{S_i - N_i}{N_i} - \frac{S_{i-1}- N_{i-1}}{N_{i-1}}\right|,
```

and a relative statistical uncertianty
```math
\delta x_{i-1,i} = \sqrt{\frac{\delta M_i^2}{N_i^2} + \frac{\delta M_{i-1}^2}{N_{i-1}^2}}.
```

If at least one bin satisfies

```math
x_{i−1,i} < \delta x_{i−1,i},
```

the smoothing algorithm looks for neighbouring bins `b − 1` and `b` with the highest ratio

```math
\frac{\delta x_{b-1,b}}{x_{b-1,b}},
```

then these bins are merged.
The process continues until there is no bin in the distribution that satisfies

```math
x_{i−1,i} < \delta x_{i−1,i}.
```

Finally, TH1::Smooth is used in the end.

More details can be found [here](https://indico.cern.ch/event/669913/contributions/2769795/attachments/1549339/2433688/ttres-fullunblind-smooth2-summary2.pdf).

### t-channel single top smoothing
The smoothing algorithm follows two main steps: A rebinning step to decrease statistical fluctuations and a smoothing step to interpolate back to the original binning.
In the rebinning step, the systematic uncertainty template `s`and the nominal template `n` are temporarily rebinned by step-wise merging of adjacent bins, reducing the number of bins from `N_o` to `N_r`.
During the rebinning, the rebinned version of `s` and `n` are referenced by `\tilde{s}` and `\tilde{n}` respectively.
The bin-wise ratios 
```math
t_i = s_i/n_i, \\
\tilde{t}_i = \tilde{s}_{j(i)}/\tilde{n}_{j(i)}
```
are computed where `i = 1, . . . , N_o` and `j(i)` gives the bin in the rebinned templates containing bin `i` of the original
template.
`\tilde{t}` therefore has the same value for bins that got merged into the same bin in the rebinning.
Two rules are implemented to focus the rebinning on removing statistical fluctuations and prevent the removal of shape effects: Firstly, two adjacent bins are only allowed to be merged if in `\tilde{t}` one of them has a higher or lower value than both of its adjacent bins, thus preventing the merging of bins where `t` is monotonously falling or rising.
Secondly, bins are not allowed to be merged if the resulting `\tilde{t}` has three consecutive bins all with a higher or all with a lower value compared to `t`.
If multiple bin pairs are allowed to be merged after applying the two rules, the one is merged where after merging

```math
D = \sup_{j = 1,...,N_o}\left(\left|\sum_{i=1}^j \tilde{t}_i-t_i\right|\right),
```

would be the smallest.

After this, the rebinned ratio template `\tilde{t}` is smoothed using a slightly modified version of the smoothing algorithm TH1::smooth().
The ROOT smoothing algorithm is consecutively applied three times on `\tilde{t}` and consists of running medians of 3,5 and 3 applied successively, quadratic interpolation, a running mean and twicing.
The algorithm is modified to leave the first and last bin unchanged in the running medians during the first application of the smoothing.
From this smoothed ratio template the smoothed systematic uncertainty template is calculated by multiplying bin-wise with the original nominal template.
