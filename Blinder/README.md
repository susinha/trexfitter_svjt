# Blinder [![build status](https://gitlab.cern.ch/TRExStats/Blinder/badges/master/pipeline.svg "build status")](https://gitlab.cern.ch/TRExStats/Blinder/commits/master)

This repository holds a simple code that can be used for blinding a value in a meaasurement by turning it into a string that is not human readable, but can be read by a machine. 
The code also provides a convenient class that can make plots that just show the shift of the values without showing their mean values.

## Main idea
The main idea of the code is to split a value (double) into parts before and after the decimal separator and count number of zeroes.
This leaves three integer numbers (before the decimal separator, number of zeroes after the separator and then the rest of the number).
Some predefined values are added to all three of these numbers and then the final result is transformed into a hexa-decimal number making it pretty hard for humans to read.
However, since all of the steps are predefined, the process can be reverted, thus the blinded value is machine-readable.

## Prerequisites
The code only requires cmake and ROOT. If you are on lxplus all you need is
```
setupATLAS
lsetup cmake
lsetup "views LCG_99 x86_64-centos7-gcc8-opt"
```

## Cloning and compilation
To clone the repository do
```
git clone ssh://git@gitlab.cern.ch:7999/TRExStats/Blinder.git
```

Then `cd` to the repository and create a `build` directory
```
mkdir build
```

To compile the code do
```
cd build
cmake ../
make
```

If you are using cmake in your code, you need to add the following lines your make CMakeLists.txt file
```
# Add the blinder code
include_directories( Blinder )
add_subdirectory( Blinder )
```

And then link against this library in the code (you probably already have a similar line somewhere)
```
target_link_libraries( YourProjectName Blinder ${ROOT_LIBRARIES} )
```

## Using the Blinder
If you only want to use the function that blind a value and then read back the string, all you need to do is to include the header in your code and then call the functions.

```c++
#include "Blinder/Blinder.h"

...
// blind the value
const std::string alt1Meas = Blinder::DoubleToPseudoHex(1.20)

...

// get back the value
const double origAlt1 = Blinder::HexToDouble(alt1Meas);
```

## Using the BlinderPlotter

To use the plotter, you need to initialize the class and then provide the meaasurements that you want to compare.

```c++
// initilize the class
Blinder::BlinderPlotter plotter{};

// add the default values, i.e. the nominal setup
plotter.AddDefaultMeasurement(1.25, 0.15, -0.16);
// can also use blinded version instead
// plotter.AddDefaultMeasurementBlind(defaultMeas, 0.15, -0.16);

// add the alternative setups
plotter.AddAlternative("alt1", 1.20, 0.15, -0.16);

// notice that you can add it also with the blinded values directly
plotter.AddAlternativeBlind("alt2", alt2Meas, 0.14, -0.13);

// plot the differences, first parameter is the x-axis title, the second parameter is the path
plotter.Plot("#Delta #mu", "example.png");
```

## Example
A simple example executable is provided [here](https://gitlab.cern.ch/TRExStats/Blinder/-/blob/master/util/example.cc) which shows how to use the functions and the plotter class.

To run the simple example, simply do in the main folder

```
./build/bin/example
```
