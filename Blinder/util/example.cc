#include "Blinder/Blinder.h"
#include "Blinder/BlinderPlotter.h"

#include <iostream>
#include <string>

int main(int /*argc*/, char** /*argv*/) {

  std::cout << "Imagine we have a measurement that has these three results:\n\n";
  std::cout << "default: 1.25 +0.15 -0.16\n";
  std::cout << "alternative1: 1.20 +0.15 -0.16\n";
  std::cout << "alternative2: 1.21 +0.14 -0.13\n";

  // Now we want to blind the mean value
  const std::string defaultMeas = Blinder::DoubleToPseudoHex(1.25);
  const std::string alt1Meas = Blinder::DoubleToPseudoHex(1.20);
  const std::string alt2Meas = Blinder::DoubleToPseudoHex(1.21);

  std::cout << "\nBlinded results are:\n";
  std::cout << "Default: " << defaultMeas << " +0.15 -0.16\n";
  std::cout << "Alternative1: " << alt1Meas << " +0.15 -0.16\n";
  std::cout << "Alternative2: " << alt2Meas << " +0.14 -0.13\n";

  // try do decore it now
  const double origDefault = Blinder::HexToDouble(defaultMeas);
  const double origAlt1 = Blinder::HexToDouble(alt1Meas);
  const double origAlt2 = Blinder::HexToDouble(alt2Meas);

  std::cout << "\nOriginal values are\n";
  std::cout << "Default: " << origDefault << " +0.15 -0.16\n";
  std::cout << "Alternative1: " << origAlt1 << " +0.15 -0.16\n";
  std::cout << "Alternative2: " << origAlt2 << " +0.14 -0.13\n";

  // now plot it
  Blinder::BlinderPlotter plotter{};
  plotter.AddDefaultMeasurement(1.25, 0.15, -0.16);
  // can use blinded one instead
  // plotter.AddDefaultMeasurementBlind(defaultMeas, 0.15, -0.16);
  plotter.AddAlternative("alt1", 1.20, 0.15, -0.16);
  plotter.AddAlternativeBlind("alt2", alt2Meas, 0.14, -0.13);
  plotter.Plot("#Delta #mu", "example.png");

  return 0;
}
