#pragma once

#include <string>

namespace Blinder {

    /**
     * Function that takes a value and returns a string that is not human readable.
     * This can be used to blind the mean value of the measurement
     * @param value The value to be blinded
     * @return Returns std::string that is the blided value
     */
    std::string DoubleToPseudoHex(double value);

    /**
     * Function to take the blinded string and undo the blinding to get the original value.
     * @param s The blinded parameter
     * @return The original value
     */
    double HexToDouble(const std::string& s);
}
