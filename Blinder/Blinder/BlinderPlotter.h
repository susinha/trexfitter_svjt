#pragma once

#include <string>
#include <vector>

namespace Blinder {

class BlinderPlotter {
public:

    /**
     * A simple struct to hold some values for the measurement
     */
    struct Measurement {
        std::string name;
        double mean;
        double errUp;
        double errDown;
    };

    explicit BlinderPlotter();
    ~BlinderPlotter() = default;

    BlinderPlotter(const BlinderPlotter& p) = delete;
    BlinderPlotter operator=(BlinderPlotter& p) = delete;
    BlinderPlotter(const BlinderPlotter&& p) = delete;
    BlinderPlotter operator=(BlinderPlotter&& p) = delete;

    /**
     * Adding the default (nominal setup) values
     * @param mean the mean value
     * @param up the up error
     * @param down the down error
     */
    void AddDefaultMeasurement(const double mean, const double up, const double down);

    /**
     * Adding the default (nominal setup) values
     * @param mean the blinded mean value
     * @param up the up error
     * @param down the down error
     */
    void AddDefaultMeasurementBlind(const std::string& mean, const double up, const double down);

    /**
     * Adding the alternative (test setup) values
     * @param name the name of the setup
     * @param mean the mean value
     * @param up the up error
     * @param down the down error
     */
    void AddAlternative(const std::string& name, const double nominal, const double up, const double down);
    
    /**
     * Adding the alternative (test setup) values now with the blinded mean
     * @param name the name of the setup
     * @param mean string of the blinded mean value
     * @param up the up error
     * @param down the down error
     */
    void AddAlternativeBlind(const std::string& name, const std::string& nominal, const double up, const double down);

    /**
     * Method to to plot the differences
     * @param title of the x-axis, e.g. "#Delte #mu"
     * @param path the full path to the output file also with the extension (will be calles in SaveAs() for the canvas)
     */
    void Plot(const std::string& title, const std::string& path) const;

private:
    bool m_hasDefault;
    bool m_hasAlternative;
    Measurement m_default;
    std::vector<Measurement> m_alternative;
};
}
