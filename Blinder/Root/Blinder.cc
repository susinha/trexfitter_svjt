#include "Blinder/Blinder.h"

#include <sstream>

std::string Blinder::DoubleToPseudoHex(double value){
    value+= 31415;
    std::string s = std::to_string(value);
    std::string first = s.substr(0,s.find('.'));
    std::string second = s.substr(s.find('.')+1, s.length());

    //Count the number of "0" after the comma
    int count = 0.;
    for (unsigned int i = 0; i < second.size(); i++) {
        if (second[i] != '0') {
            break;
        }
        count++;
    }

    int value1 = std::stoi(first);
    if (first == "-0") {
      value1 = -0;
    }
    const int value2 = std::stoi(second);

    // add 1234 to the first digit so it is not easily readable, we will subtract it in the decoding
    value1+=1234;
    // add 5678 to the number of '0'
    count+=5678;

    std::stringstream ss;
    ss << std::hex << value1 << "." << std::hex << count  << "." << std::hex << value2;

    return ss.str();
}

double Blinder::HexToDouble(const std::string& s){
    std::string first = s.substr(0,s.find('.'));
    std::string rest = s.substr(s.find('.')+1, s.length());
    std::string zeros = rest.substr(0,rest.find('.'));
    std::string second = rest.substr(rest.find('.')+1, rest.length());

    std::size_t i1, i2, n0;

    std::stringstream ss;
    ss << std::hex << first;
    ss >> i1;

    std::stringstream ss1;
    ss1 << std::hex << second;
    ss1 >> i2;

    std::stringstream ss2;
    ss2 << std::hex << zeros;
    ss2 >> n0;

    int signed1 = static_cast<int>(i1);
    // need to subtract the 1234 we added
    signed1-= 1234;
    // need to substract the 5678
    n0-= 5678;

    std::string result = std::to_string(signed1)+".";

    for (std::size_t i = 0; i < n0; i++) {
      result += "0";
    }

    result += std::to_string(i2);

    return std::stod(result) - 31415;
}
