#include "Blinder/BlinderPlotter.h"
#include "Blinder/Blinder.h"

#include "TCanvas.h"
#include "TGraphAsymmErrors.h"
#include "TH2D.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStyle.h"

#include <algorithm>
#include <iostream>

namespace Blinder{

BlinderPlotter::BlinderPlotter() :
m_hasDefault(false),
m_hasAlternative(false)
{
}

void BlinderPlotter::AddDefaultMeasurement(const double mean, const double up, const double down) {
    if (m_hasDefault) {
        std::cout << "Warning: Default measurement has already been added, will overwrite it now!";
    }
    BlinderPlotter::Measurement meas;
    meas.name = "Default";
    meas.mean = mean;
    meas.errUp = up;
    meas.errDown = down;
    m_default = meas;
    m_hasDefault = true;
}

void BlinderPlotter::AddDefaultMeasurementBlind(const std::string& mean, const double up, const double down) {
    if (m_hasDefault) {
        std::cout << "Warning: Default measurement has already been added, will overwrite it now!";
    }
    BlinderPlotter::Measurement meas;
    meas.name = "Default";
    meas.mean = Blinder::HexToDouble(mean);
    meas.errUp = up;
    meas.errDown = down;
    m_default = meas;
    m_hasDefault = true;
}

void BlinderPlotter::AddAlternative(const std::string& name, const double mean, const double up, const double down) {

    // Fist check of the name is already added
    auto it = std::find_if(m_alternative.begin(), m_alternative.end(), [&name](const BlinderPlotter::Measurement& m){return m.name == name;});
    if (it != m_alternative.end()) {
        std::cerr << "BlinderPlotter::AddAlternative: Measurement with name: " << name << " is alredy in the list, ignoring\n";
        return;
    }

    BlinderPlotter::Measurement meas;
    meas.name = name;
    meas.mean = mean;
    meas.errUp = up;
    meas.errDown = down;

    m_alternative.emplace_back(std::move(meas));

    m_hasAlternative = true;
}

void BlinderPlotter::AddAlternativeBlind(const std::string& name, const std::string& mean, const double up, const double down) {

    // Fist check of the name is already added
    auto it = std::find_if(m_alternative.begin(), m_alternative.end(), [&name](const BlinderPlotter::Measurement& m){return m.name == name;});
    if (it != m_alternative.end()) {
        std::cerr << "BlinderPlotter::AddAlternativeBlind: Measurement with name: " << name << " is alredy in the list, ignoring\n";
        return;
    }

    BlinderPlotter::Measurement meas;
    meas.name = name;
    meas.mean = Blinder::HexToDouble(mean);
    meas.errUp = up;
    meas.errDown = down;

    m_alternative.emplace_back(std::move(meas));

    m_hasAlternative = true;
}

void BlinderPlotter::Plot(const std::string& title, const std::string& path) const {
    if (!m_hasDefault) {
        std::cerr << "BlinderPlotter::Plot: No default measurement set. Exitting\n";
        return;
    }

    if (!m_hasAlternative) {
        std::cerr << "BlinderPlotter::Plot: No alternatice measurement set. Exitting\n";
        return;
    }

    // make the plot
    TCanvas c{};
    c.SetGridy();
    c.SetBottomMargin(0.15);

    const size_t n = m_alternative.size()+1;
    std::vector<double>x(n);
    std::vector<double>y(n);
    std::vector<double>hi(n);
    std::vector<double>lo(n);
    std::vector<double>eyhi(n);
    std::vector<double>eylo(n);
    x[0] = 0;
    y[0] = n - 0.5;
    hi[0] = m_default.errUp;
    lo[0] = std::abs(m_default.errDown);
    eyhi[0] = 0;
    eylo[0] = 0;
    for (std::size_t i = 0; i < n - 1; i++) {
        x[i+1] = m_alternative.at(i).mean - m_default.mean;
        y[i+1] = n-1.5-i;
        hi[i+1] = m_alternative.at(i).errUp;
        lo[i+1] = std::abs(m_alternative.at(i).errDown);
        eyhi[i+1] = 0;
        eylo[i+1] = 0;
    }

    double maxValue = *std::max_element(x.begin(), x.end(), [](const double& a, const double& b){return std::abs(a) < std::abs(b);});
    double maxError = *std::max_element(hi.begin(), hi.end());
    const double range = 1.3*(std::abs(maxValue) + maxError);

    TH2D h("","",1,-range,range,n,0,n);
    for (std::size_t i = 0; i < n-1; i++) {
        std::string binTitle = m_alternative.at(n-i-2).name;
        h.GetYaxis()->SetBinLabel(i+1,binTitle.c_str());
    }
    h.GetYaxis()->SetBinLabel(n,m_default.name.c_str());
    gStyle->SetOptStat(0);
    h.GetYaxis()->SetRangeUser(0,n+3);

    TGraphAsymmErrors gr(n,x.data(),y.data(),lo.data(),hi.data(),eylo.data(),eyhi.data());
    const double lsize = h.GetYaxis()->GetLabelSize();
    h.GetYaxis()->SetLabelSize(1.5*lsize);
    h.GetXaxis()->SetTitle(title.c_str());
    const double xsize = h.GetXaxis()->GetTitleSize();
    h.GetXaxis()->SetTitleSize(1.3*xsize);

    std::vector<double> x_dum(n, 0);
    std::vector<double> y_dum(n, 0);
    std::vector<double> xerrhi_dum(n, hi[0]);
    std::vector<double> xerrlo_dum(n, lo[0]);
    std::vector<double> yerrhi_dum(n, 100);
    std::vector<double> yerrlo_dum(n, 100);
    TGraphAsymmErrors gr_dum(1,x_dum.data(),y_dum.data(),xerrlo_dum.data(),xerrhi_dum.data(),yerrlo_dum.data(),yerrhi_dum.data());

    gPad->SetLeftMargin(0.2);

    h.Draw();

    gr.SetMarkerStyle(8);
    gr_dum.SetFillColorAlpha(kGreen+1,0.5);

    gr_dum.Draw("2");
    gr.SetLineWidth(2);
    gr.Draw("P");

    TLatex latex{};
    latex.SetTextFont(62);
    latex.SetTextSize(0.05);
    latex.SetNDC();
    latex.DrawLatex(0.25,0.85,"ATLAS");

    TLatex latex1{};
    latex1.SetTextFont(42);
    latex1.SetTextSize(0.05);
    latex1.SetNDC();
    latex1.DrawLatex(0.38,0.85,"Internal");

    c.SaveAs(path.c_str());    
}

}