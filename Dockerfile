FROM gitlab-registry.cern.ch/atlas/statanalysis:0-2-4

COPY . /code/src/TRExFitter

# we need to remove the build directory as it is leftover from the compile step of the CI
RUN source /release_setup.sh

ENTRYPOINT ["/bin/bash", "-l", "-c"]
CMD ["/bin/bash"]

