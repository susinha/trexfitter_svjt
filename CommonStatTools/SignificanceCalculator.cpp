#include "SignificanceCalculator.h"
#include "Minimization.h"

#include "Math/MinimizerOptions.h"
#include "RooAbsData.h"
#include "RooAbsReal.h"
#include "RooArgSet.h"
#include "RooDataSet.h"
#include "RooRandom.h"
#include "RooRealVar.h"
#include "RooWorkspace.h"

// RooStats includes
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodTestStat.h"
#include "RooStats/ToyMCSampler.h"

#include "TFile.h"
#include "TMath.h"
#include "TTree.h"

#include <iostream>
#include <memory>

namespace CommonStatTools {

SignificanceCalculator::SignificanceCalculator() noexcept :
m_significance(-1.),
m_pValue(999),
m_nll_mu0(-1.),
m_nll_mu(-1.),
m_seed(1234567),
m_cpu(1),
m_printout(10)
{
   ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
   ROOT::Math::MinimizerOptions::SetDefaultStrategy(1);
   ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(-1);
}
  
void SignificanceCalculator::SetCPU(const int cpu) {
  if (cpu < 1) {
    std::cout << "SignificanceCalculator::SetCPU: Number of CPUs requested is < 1, setting to 1\n";
    m_cpu = 1;
    return;
  }

  m_cpu = cpu;
}

void SignificanceCalculator::CalculateSignificance(RooStats::ModelConfig* mc, RooDataSet* data) {
  if (!mc || !data) {
    throw std::invalid_argument{"SignificanceCalculator::CalculateSignificance: ModelConfig or RooDataSet is nullptr"};
  }

  const RooArgSet* glbObs = mc->GetGlobalObservables();

  RooAbsPdf* pdf = mc->GetPdf();

  std::unique_ptr<RooAbsReal> nll(pdf->createNLL(
                                    *data,
                                    RooFit::GlobalObservables(*glbObs),
                                    RooFit::Offset(kTRUE),
                                    RooFit::NumCPU(m_cpu,RooFit::Hybrid),
                                    RooFit::Optimize(kTRUE))
                                  );

  RooRealVar* mu = static_cast<RooRealVar*>(mc->GetParametersOfInterest()->first()); 
  if (!mu) {
    throw std::runtime_error{"SignificanceCalculator::CalculateSignificance: The POI is nullptr"};
  }

  // set the mu to 0 and fix
  mu->setVal(0);
  mu->setConstant(kTRUE);

  int status = EXOSTATS::minimize(nll.get());
  if (status < 0){
    std::cerr << "SignificanceCalculator::CalculateSignificance: Minimisation for mu = 0 failed\n";
    return;
  }

  m_nll_mu0 = nll->getVal();

  // run the normal fit with floating mu
  mu->setConstant(kFALSE);
  
  status = EXOSTATS::minimize(nll.get());
  if (status < 0){
    std::cerr << "SignificanceCalculator::CalculateSignificance: Minimisation for floating mu failed\n";
    return;
  }

  m_nll_mu = nll->getVal();

  // calculate the significance
  m_significance = SignificanceCalculator::SigFromNLL(m_nll_mu0, m_nll_mu);
  m_pValue = SignificanceCalculator::SigToPvalue(m_significance);
}
  
void SignificanceCalculator::CalculateSignificanceToys(RooWorkspace* ws,
                                                       RooStats::ModelConfig* mc,
                                                       RooDataSet* data,
                                                       const int toys) {

  if (!mc || !data) {
    throw std::invalid_argument{"SignificanceCalculator::CalculateSignificanceToys: ModelConfig or Data is nullptr"};
  }

  if (toys < 1) {
    throw std::invalid_argument{"SignificanceCalculator::CalculateSignificanceToys: Number of toys < 1"};
  }
  
  m_sigToys.resize(toys);
  m_pValueToys.resize(toys);

  const RooArgSet* glbObs = mc->GetGlobalObservables();
  
  ws->saveSnapshot("original",*glbObs);

  RooAbsPdf* pdf = mc->GetPdf();
  
  RooDataSet* dummy = pdf->generate(*mc->GetObservables(), RooFit::Extended());

  std::unique_ptr<RooAbsReal> nll(pdf->createNLL(
                                    *dummy,
                                    RooFit::GlobalObservables(*glbObs),
                                    RooFit::Offset(kTRUE),
                                    RooFit::NumCPU(m_cpu,RooFit::Hybrid),
                                    RooFit::Optimize(kTRUE))
                                  );

  RooRealVar* mu = static_cast<RooRealVar*>(mc->GetParametersOfInterest()->first()); 
  if (!mu) {
    throw std::runtime_error{"SignificanceCalculator::CalculateSignificanceToys: The POI is nullptr"};
  }

  // randomize GlobalObservables using ToyMCSampler
  RooStats::ProfileLikelihoodTestStat ts(*pdf);
  RooStats::ToyMCSampler sampler(ts,toys);
  sampler.SetPdf(*pdf);
  sampler.SetObservables(*mc->GetObservables());
  sampler.SetGlobalObservables(*glbObs);
  sampler.SetParametersForTestStat(*mc->GetParametersOfInterest());
  sampler.SetUseMultiGen(false);
  RooStats::ToyMCSampler::SetAlwaysUseMultiGen(false);

  RooArgSet poiAndNuisance;
  poiAndNuisance.add(*mu);
  poiAndNuisance.add(*mc->GetNuisanceParameters());
  RooArgSet* nullParams = static_cast<RooArgSet*>(poiAndNuisance.snapshot());
  ws->saveSnapshot("paramsToFitPE",poiAndNuisance);
    
  // loop over the toys
  for (int itoy = 0; itoy < toys; ++itoy) {
    ws->loadSnapshot("paramsToFitPE");

    // set random value - even when the fit fails
    auto&& rnd = RooRandom::randomGenerator();
    rnd->SetSeed(m_seed + itoy);

    // generate random data
    std::unique_ptr<RooDataSet> toyData(static_cast<RooDataSet*>(sampler.GenerateToyData(*nullParams)));
    nll->setData(*toyData);

    mu->setVal(0);
    mu->setConstant(kTRUE);

    // get the nll0 value
    int status = EXOSTATS::minimize(nll.get());
    if (status < 0){
      std::cout << "SignificanceCalculator::CalculateSignificanceToys: Minimisation for mu = 0 failed for fit " << itoy << ", returning -999\n";
      m_sigToys.at(itoy) = -999.;
      m_pValueToys.at(itoy) = 999.;
      continue;
    }
    const double nll0 = nll->getVal();

    // now run the fit with floating mu
    mu->setConstant(kFALSE);
    status = EXOSTATS::minimize(nll.get());
    if (status < 0){
      std::cout << "SignificanceCalculator::CalculateSignificanceToys: Minimisation for floating failed for fit " << itoy << ", returning -999\n";
      m_sigToys.at(itoy) = -999.;
      m_pValueToys.at(itoy) = 999.;
      continue;
    }

    const double minNLL = nll->getVal();
    const float sig = SignificanceCalculator::SigFromNLL(nll0, minNLL);
    m_sigToys.at(itoy) = sig;
    m_pValueToys.at(itoy) = SignificanceCalculator::SigToPvalue(sig);

    // write something to the console to see the progress
    if (m_printout > 0 && (itoy % m_printout == 0)) {
      std::cout << "SignificanceCalculator::CalculateSignificanceToys: Toy " << itoy << " out of " << toys << " toys\n";
      std::cout << "SignificanceCalculator::CalculateSignificanceToys: \t significance: " << sig << "\n";
      std::cout << "SignificanceCalculator::CalculateSignificanceToys: \t p-value: " << m_pValueToys.at(itoy) << "\n";
    }
  } // loop over toys

  // restore the original WS
  ws->loadSnapshot("original");
}

void SignificanceCalculator::WriteResultsToROOTfile(TFile* file, const std::string& param) const {
  if (!file) {
    throw std::invalid_argument{"SignificanceCalculator::WriteResultsToROOTfile: File is nullptr"};
  }

  TTree tree(param.c_str(), "");
  Float_t pvalue = m_pValue;
  Float_t sig    = m_significance;
  Float_t nll0   = m_nll_mu0;
  Float_t nll    = m_nll_mu;

  tree.Branch("significance",&sig);
  tree.Branch("pValue",&pvalue);
  tree.Branch("nll0",&nll0);
  tree.Branch("nll",&nll);

  tree.Fill();

  tree.SetDirectory(file);
  file->Write();
}

void SignificanceCalculator::WriteToysToROOTfile(TFile* file, const std::string& param) const {
  if (!file) {
    throw std::invalid_argument{"SignificanceCalculator::WriteToysToROOTfile: File is nullptr"};
  }

  TTree tree(param.c_str(), "");
  Float_t pvalue{};
  Float_t sig{};

  tree.Branch("significance",&sig);
  tree.Branch("pValue",&pvalue);

  for (std::size_t itoy = 0; itoy < m_sigToys.size(); ++itoy) {
    sig    = m_sigToys.at(itoy);
    pvalue = m_pValueToys.at(itoy);
    tree.Fill();
  }

  tree.SetDirectory(file);
  file->Write();
}

float SignificanceCalculator::SigFromNLL(const double nll0, const double nll) {
  const double q0 = 2*(nll0 - nll);
  if (q0 < 0) return -std::sqrt(-q0);

  return std::sqrt(q0);
}

float SignificanceCalculator::SigToPvalue(const float sig) {
  static const float oversqrt2 = 1./std::sqrt(2.);
  return 0.5 * (1. - TMath::Erf(sig * oversqrt2));
}

float SignificanceCalculator::PvalueToSig(const float pValue) {
  return ROOT::Math::normal_quantile_c(pValue, 1.);
}

}
