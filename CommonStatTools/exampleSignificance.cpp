#include "SignificanceCalculator.h"

#include "RooDataSet.h"
#include "RooWorkspace.h"
#include "RooStats/ModelConfig.h"

#include "TFile.h"

#include <iostream>
#include <memory>
#include <string>

int main(int /*argc*/, char** /*argv*/) {

  // prepare the inputs
  static const std::string wsPath = "/afs/cern.ch/user/v/vippolit/public/workspace.root";
  std::unique_ptr<TFile> wsFile(TFile::Open(wsPath.c_str(), "READ"));
  if (!wsFile) {
    std::cerr << "Cannot open the input file!\n";
    return 1;
  }

  // get the WS, ModelConfig, data
  RooWorkspace* ws = dynamic_cast<RooWorkspace*>(wsFile->Get("combined"));
  if (!ws) {
    std::cerr << "Invalid workspace!\n";
    return 1;
  }
  RooStats::ModelConfig* mc = static_cast<RooStats::ModelConfig*>(ws->obj("ModelConfig"));
  RooDataSet* data = static_cast<RooDataSet*>(ws->data("obsData"));

  if (!data || !mc) {
    std::cerr << "Invalid ModelConfig or data\n";
    return 1;
  }

  // prepare the significance class
  CommonStatTools::SignificanceCalculator calculator{};
  // set the number of CPUs used in the minimisation
  calculator.SetCPU(1);

  // calculate the significance
  calculator.CalculateSignificance(mc, data);

  // retrieve the significance
  std::cout << "Significance is: " << calculator.GetSignificance() << ", p-value is: " << calculator.GetPvalue() << "\n";

  // now use it to run toys
  // first, set the seed, the seed of each pseudoexperiment is set as the initial seed + index of the PE
  calculator.SetSeed(1337);

  // you can also set the frequency of printout, setting to -1 will disable it
  calculator.SetPrintoutFrequency(10);

  // run the toys, for this purpose just running 10 toys
  calculator.CalculateSignificanceToys(ws, mc, data, 10);

  // you can retrieve the results like this
  const std::vector<float> pValues       = calculator.GetToysPvalues();
  const std::vector<float> significances = calculator.GetToysSignificances();

  if (pValues.size() != significances.size()) {
    std::cerr << "sizes of p-values and significances do not match\n";
    return 1;
  }

  // print the fitted values
  for (std::size_t i = 0; i < pValues.size(); ++i) {
    std::cout << "toy n." << i << ": p-value: " << pValues.at(i) << ", significance: " << significances.at(i) << "\n";
  }

  // or you can store the results in a file
  std::unique_ptr<TFile> out(TFile::Open("output.root", "RECREATE"));

  // the second argument is the name of the TTree
  calculator.WriteResultsToROOTfile(out.get(), "parameterName");

  out->Close();

  // no issues
  return 0;
}
