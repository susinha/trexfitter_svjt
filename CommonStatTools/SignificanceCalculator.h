#pragma once

#include <string>
#include <vector>

/// forward class declarations
namespace RooStats {
    class ModelConfig;
}
class RooDataSet;
class RooWorkspace;
class TFile;

namespace CommonStatTools {

/**
 * @brief Class that calculates a significance from ModelConfig and Data 
 * 
 */
class SignificanceCalculator {

public: 
  /**
   * @brief Construct a new Significance Calculator object
   * 
   */
  explicit SignificanceCalculator() noexcept;

  /**
   * @brief Destroy the Significance Calculator object
   * 
   */
  ~SignificanceCalculator() = default;

  /**
   * @brief Return the significance
   * 
   * @return the significance
   */
  inline float GetSignificance() const {return m_significance;}
  
  /**
   * @brief Get the p-value
   * 
   * @return p-value
   */
  inline float GetPvalue() const {return m_pValue;}
  
  /**
   * @brief Return the NLL value with mu fixed to 0
   * 
   * @return the NLL value
   */
  inline double GetNllMu0() const {return m_nll_mu0;}
  
  /**
   * @brief Return the NLL value with mu free floating and fitted to its best value
   * 
   * @return the NLL value
   */
  inline double GetNllMu() const {return m_nll_mu;}

  /**
   * @brief Set the seed for the random numbers
   * 
   * @param seed 
   */
  inline void SetSeed(const long int seed) {m_seed = seed;}

  /**
   * @brief Get the vector of significances, will be -999 for a failed fit
   * 
   * @return the significances
   */
  inline const std::vector<float>& GetToysSignificances() const {return m_sigToys;}

  /**
   * @brief Get the vector of p-values, will have 999 for a failed fit
   * 
   * @return the p-values
   */
  inline const std::vector<float>& GetToysPvalues() const {return m_pValueToys;}

  /**
   * @brief Set number of CPUs used for the minimisation 
   * 
   * @param cpu number of cpus
   */
  void SetCPU(const int cpu);

  /**
   * @brief Set the frequency of printout for toys, if set to value < 0 will disable printout.
   * Setting to e.g. 10 will print it every 10 pseudoexperiments
   * 
   * @param n 
   */
  void SetPrintoutFrequency(const int n){m_printout = n;}

  /**
   * @brief The main method that does the calculation
   * 
   * @param mc ModelConfig
   * @param data The dataset - can be asimov/real data based on what is passed
   */
  void CalculateSignificance(RooStats::ModelConfig* mc, RooDataSet* data);

  /**
   * @brief Calculation of significance from toys
   * 
   * @param ws Workspace
   * @param mc ModelConfig
   * @param data The dataset - can be asimov/real data based on what is passed
   * @param toys the number of toys
   */
  void CalculateSignificanceToys(RooWorkspace* ws, RooStats::ModelConfig* mc, RooDataSet* data, const int toys);

  /**
   * @brief Stores the results of significance and p-value in a ROOT file 
   * 
   * @param file The ROOT file 
   * @param param name of the TTree
   */
  void WriteResultsToROOTfile(TFile* file, const std::string& param) const;
  
  /**
   * @brief Stores the results of toys for significance and p-value in a ROOT file 
   * 
   * @param file The ROOT file 
   * @param param name of the TTree
   */
  void WriteToysToROOTfile(TFile* file, const std::string& param) const;

  /**
   * @brief Return significance from the two NLL values - with mu fixed at 0 and with mu floating 
   * 
   * @param nll0 NLL value with mu fixed at 0
   * @param nll NLL value with mu floating
   * @return significance
   */
  static float SigFromNLL(const double nll0, const double nll);
  
  /**
   * @brief Helper function to turn significance value to p-value 
   * 
   * @param sig significance
   * @return corresponding p-value
   */
  static float SigToPvalue(const float sig);
  
  /**
   * @brief Helper function to turn p-value to significance 
   * 
   * @param pValue 
   * @return significance
   */
  static float PvalueToSig(const float pValue);

private:

  double m_significance;
  double m_pValue;
  double m_nll_mu0;
  double m_nll_mu;
  long int m_seed;
  int m_cpu;
  int m_printout;
  std::vector<float> m_sigToys;
  std::vector<float> m_pValueToys;

};

}
