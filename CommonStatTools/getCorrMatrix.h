#ifndef __EXOSTATS__getCorrMatrix_h__
#define __EXOSTATS__getCorrMatrix_h__
#include <TString.h>

void getCorrMatrix(const char *inputFile, const char *workspaceName, const char *modelConfigName, const char *dataName, TString workspaceTag, TString outputFolder, TString outputFormat, Bool_t doReduced = kTRUE, Int_t debugLevel = 2);
#endif
